import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trans } from './trans';

@Injectable({
  providedIn: 'root'
})
export class BillsService {

  _url = 'http://localhost:3000/transactions';
  _url2 = 'http://localhost:3000/cashTrans';

  constructor(private _http: HttpClient) { }

  save(trans: Trans) {
    return this._http.post<any>(this._url, trans);
  }
  getAllTrans() {
    return this._http.get(this._url2);
  }
}
