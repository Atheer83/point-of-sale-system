import { Component, OnInit } from '@angular/core';
import { BillsService } from './../bills.service';
import {  ActivatedRoute } from '@angular/router';
import { Trans } from '../trans';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  public cashierId;
  public id;
  transModel = new Trans(0, 0, 0, 0);
  amount = this.transModel.amount;
  paid = this.transModel.paid;

  onClick(amount, paid) {
    this.transModel.changeAmount = paid - amount;
    this.transModel.cashierId = this.id;
  }
  constructor(
    private _billsService: BillsService,
    private route: ActivatedRoute
  ) { }

  onSubmit() {
      console.log(this.transModel);
      this._billsService.save(this.transModel)
      .subscribe(
        data => console.log('Success', data),
        error => console.error('Error', error)
      );
    }
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.cashierId = this.id;
  }

}
