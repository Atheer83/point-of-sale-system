import { Component, OnInit } from '@angular/core';
import { BillsService } from '../bills.service';

@Component({
  selector: 'app-cashier-transactions',
  templateUrl: './cashier-transactions.component.html',
  styleUrls: ['./cashier-transactions.component.scss']
})
export class CashierTransactionsComponent implements OnInit {
  logs: any;
  val;
  cashiers = [
    {id: 1, name: 'Cashier1', email: 'cashier1@gmail.com'},
    {id: 2, name: 'Cashier2', email: 'cashier2@gmail.com'}
  ];


  constructor(private _transService: BillsService) { }

  ngOnInit() {
}

getTrans(e) {
  this.val = e.target.value;
    this._transService.getAllTrans().subscribe(
    _transService => { console.log(_transService); this.logs = _transService;
    }
    );
  }
}
