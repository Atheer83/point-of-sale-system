import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashierTransactionsComponent } from './cashier-transactions.component';

describe('CashierTransactionsComponent', () => {
  let component: CashierTransactionsComponent;
  let fixture: ComponentFixture<CashierTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashierTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashierTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
