export class Trans {
  constructor(
    public cashierId: number,
    public amount: number,
    public paid: number,
    public changeAmount: number,
    public current: Date = new Date
  ) {}}
