import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cashier-login',
  templateUrl: './cashier-login.component.html',
  styleUrls: ['./cashier-login.component.scss']
})
export class CashierLoginComponent implements OnInit {
  // cashiers = [
  //   {id: 1, name: 'Cashier1', email: 'cashier1@gmail.com'},
  //   {id: 2, name: 'Cashier2', email: 'cashier2@gmail.com'}
  // ];


  constructor(private router: Router) { }

  ngOnInit() {
  }
  onWriteEmail(name) {
    if (name === 'cashier1@gmail.com') {
      this.router.navigate(['/transactions', 1]);
  } else if (name === 'cashier2@gmail.com') {
    this.router.navigate(['/transactions', 2]);
  } else {
    (this.router.navigate(['/login']));
    }
  }
}
