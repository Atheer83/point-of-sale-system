
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashierTransactionsComponent } from './cashier-transactions/cashier-transactions.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { CashierLoginComponent } from './cashier-login/cashier-login.component';
import { MainPageComponent } from './main-page/main-page.component';


const routes: Routes = [
  {
    path: '',
    component: MainPageComponent
  },
  {
    path: 'login',
    component: CashierLoginComponent
  },
  {
    path: 'cashTrans',
    component: CashierTransactionsComponent
  },
  {
    path: 'transactions/:id',
    component: TransactionsComponent
  },
  { path: 'login/transactions',   redirectTo: 'transactions/:id', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
