const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
var mysql = require('mysql');

let con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "pointOfSell"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");

  con.query("CREATE DATABASE pointOfSell", function (err) {
    if (err) {
    console.log("Database exist");
    } else {
          console.log("Database created");
    }
  });

  let sql = 'CREATE TABLE cashiers (cashierId INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255))';
  let sql2 = 'CREATE TABLE transactions (processId INT AUTO_INCREMENT PRIMARY KEY, cashierId INT, amount VARCHAR(255), paid VARCHAR(255), changeAmount INT, current VARCHAR(255))';

  con.query(sql, function (err) {
    if (err) {
      // console.log(err);
      } else {
            console.log("Table1 created");
      }
  });
  con.query(sql2, function (err) {
    if (err) {
      console.log(err);
      } else {
            console.log("Table2 created");
      }
  });
  let info = 'INSERT INTO cashiers (name, email) VALUES ?';
  let values = [
    ['Cashier1', 'cashier1@gmail.com'],
    ['Cashier2', 'cashier2@gmail.com'],
  ];
  con.query(info, [values], function (err) {
    if (err) throw err;
    console.log("records inserted");
  });
});

const app = express();
app.use(bodyParser.json());
app.use(cors());


app.post('/transactions', (req, res) => {
  let trans = req.body;
  let sql = 'INSERT INTO transactions SET ?';
  let query = con.query(sql, trans, (err, results) => {
    if (err) {
      // console.log(err);
      } else {
            console.log("trans saved");
      }
  });
})

app.get('/cashTrans', (req, res) => {
  console.log(req.path)
  let sql = 'SELECT * from transactions';
  let query = con.query(sql, (err, results) => {
    if (err) {
      console.log(err);
      } else {
          // console.log(results);
          res.json(results);
      }
  });
})

app.use(express.static(path.join(__dirname, 'dist/ng6-StoreProj')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname,'dist/ng6-StoreProj/index.html'))
});


const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('Server started!');
});

